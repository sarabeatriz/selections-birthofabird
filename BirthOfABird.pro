#-------------------------------------------------
#
# Project created by QtCreator 2014-05-27T18:48:53
#
#-------------------------------------------------

QT       += multimedia core gui widgets

QT_VERSION = 5.4

equals(QT_VERSION, 5){
   QT += widgets
}
equals(QT_VERSION, 4) {
   QT += gui
}

CONFIG += c++11

TARGET = BirthOfABird
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    bird.cpp 

HEADERS  += mainwindow.h \
    bird.h

FORMS    += mainwindow.ui

RESOURCES += birth.qrc
